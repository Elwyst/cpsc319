//
//  PlayLayer.m
//  ChangingScenes
//
//  Created by Alysha Kwok on 12-02-05.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HighScoresLayer.h"
#import "MainMenuLayer.h"


@implementation HighScoresLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HighScoresLayer *layer = [HighScoresLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
        // background
        CCSprite *bg = [CCSprite spriteWithFile:@"paper.png"];
        //center image
        bg.position = ccp(240, 160);
        //scale image to fit screen size
        CGSize imageSize = [bg boundingBox].size;
        CGSize size = [[CCDirector sharedDirector] winSize];
        [bg setScaleX:(size.width/imageSize.width)];
        [bg setScaleY:(size.height/imageSize.height)];
        [self addChild:bg];
        
        
        // Create a button to switch screens
        CCMenuItem *mainMenuButton = [CCMenuItemImage 
                                      itemFromNormalImage:@"ButtonStar.png" selectedImage:@"ButtonStarSel.png"
                                      target: self selector:@selector(mainMenuButtonTapped)];
        mainMenuButton.position = ccp(60, 60);
        CCMenu *mainMenu = [CCMenu menuWithItems:mainMenuButton, nil];
        mainMenu.position = CGPointZero;
        [self addChild:mainMenu];
	}
	return self;
}

// method to change to main screen
- (void)mainMenuButtonTapped {
    [[CCDirector sharedDirector] replaceScene: [CCTransitionPageTurn transitionWithDuration:0.5 scene:[MainMenuLayer scene] backwards:TRUE]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// cocos2d will automatically release all the children (Label)
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
