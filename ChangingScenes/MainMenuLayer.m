//
//  HelloWorldLayer.m
//  ChangingScenes
//
//  Created by Alysha Kwok on 12-02-05.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "MainMenuLayer.h"
#import "PlayLayer.h"
#import "HighScoresLayer.h"

// HelloWorldLayer implementation
@implementation MainMenuLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuLayer *layer = [MainMenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
		// background
        CCSprite *bg = [CCSprite spriteWithFile:@"mainMenuBG.png"];
        //center image
        bg.position = ccp(240, 160);
        //scale image to fit screen size
        CGSize imageSize = [bg boundingBox].size;
        CGSize size = [[CCDirector sharedDirector] winSize];
        [bg setScaleX:(size.width/imageSize.width)];
        [bg setScaleY:(size.height/imageSize.height)];
        [self addChild:bg];
        
        // Create buttons to switch screens
        // play
        CCMenuItem *playButton = [CCMenuItemImage 
                                  itemFromNormalImage:@"start.png" selectedImage:@"start.png"
                                  target: self selector:@selector(playTapped)];      
        playButton.scale=0.5;
        
        // high score
        CCMenuItem *highScoreButton = [CCMenuItemImage
                                       itemFromNormalImage:@"highScores.png" selectedImage:@"highScores.png"
                                       target: self selector:@selector(highScoresTapped)];
        highScoreButton.scale=0.5;
        
        // credits
        CCMenuItem *creditsButton = [CCMenuItemImage
                                     itemFromNormalImage:@"credits.png" selectedImage:@"credits.png"
                                     target: self selector:@selector(creditsTapped)];
        creditsButton.scale=0.5; 
        
        // create main menu
        CCMenu *mainMenu = [CCMenu menuWithItems:playButton, highScoreButton, creditsButton, nil];
        [mainMenu setPosition:ccp(340, 120)];
        [mainMenu alignItemsVerticallyWithPadding:10];
        
        // toggle button - sound on/off buttons
        CCMenuItem *soundOnButton = [CCMenuItemImage 
                                  itemFromNormalImage:@"soundOn.png" selectedImage:@"soundOn.png"
                                   target: nil selector:nil];    
        CCMenuItem *soundOffButton = [CCMenuItemImage 
                                   itemFromNormalImage:@"soundOff.png" selectedImage:@"soundOff.png"
                                   target: nil selector:nil]; 
        soundOnButton.scale=0.5;   
        soundOffButton.scale=0.5; 
        CCMenuItemToggle *soundToggle = [CCMenuItemToggle itemWithTarget:self 
                                                                selector:@selector(soundButtonTapped:) items:soundOnButton, soundOffButton, nil];
        CCMenu *soundMenu = [CCMenu menuWithItems:soundToggle, nil];
        [soundMenu setPosition:ccp(400, 250)];
        
        // add items to layout
        [self addChild:mainMenu];
        [self addChild:soundMenu];
        
	}
	return self;
}

// method to change to play screen with page turn transition
- (void)playTapped {
    [[CCDirector sharedDirector] replaceScene: [CCTransitionPageTurn transitionWithDuration:0.5 scene: [PlayLayer scene]]];
}

- (void)highScoresTapped {
    [[CCDirector sharedDirector] replaceScene: [CCTransitionPageTurn transitionWithDuration:0.5 scene: [HighScoresLayer scene]]];
}

- (void)creditsTapped {
}

- (void)soundButtonTapped:(id)sender {
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// cocos2d will automatically release all the children (Label)
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
