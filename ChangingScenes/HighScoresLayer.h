//
//  HighScoresLayer.h
//  ChangingScenes
//
//  Created by Alysha Kwok on 12-02-05.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface HighScoresLayer : CCLayer {
    
}

// returns a CCScene that contains the HighScoresLayer as the only child
+(CCScene *) scene;

@end
