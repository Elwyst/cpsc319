//
//  PlayLayer.m
//  ChangingScenes
//
//  Created by Alysha Kwok on 12-02-05.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayLayer.h"
#import "MainMenuLayer.h"


@implementation PlayLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PlayLayer *layer = [PlayLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
		// create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Play Screen" fontName:@"Marker Felt" fontSize:64];
        
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height/2 );
		
		// add the label as a child to this Layer
		[self addChild: label];
        
        // Create a button to switch screens
        CCMenuItem *mainMenuButton = [CCMenuItemImage 
                                  itemFromNormalImage:@"ButtonStar.png" selectedImage:@"ButtonStarSel.png"
                                  target: self selector:@selector(mainMenuButtonTapped)];
        mainMenuButton.position = ccp(60, 60);
        CCMenu *mainMenu = [CCMenu menuWithItems:mainMenuButton, nil];
        mainMenu.position = CGPointZero;
        [self addChild:mainMenu];
	}
	return self;
}

// method to change to main screen
- (void)mainMenuButtonTapped {
    [[CCDirector sharedDirector] replaceScene: [MainMenuLayer scene]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// cocos2d will automatically release all the children (Label)
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
